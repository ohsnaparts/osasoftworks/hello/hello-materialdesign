import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main';

const reactRootElement = document.getElementById('react-root');
ReactDOM.render(<Main />, reactRootElement);