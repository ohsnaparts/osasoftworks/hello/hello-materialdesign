import React from 'react';

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clickCount: 0
        };
    }

    incrementClickCounter() {
        this.setState({ clickCount: this.state.clickCount+1 });
    }

    render() {
        return (<button onClick={() => this.incrementClickCounter()}>
            {this.state.clickCount}
        </button>);
    }
}