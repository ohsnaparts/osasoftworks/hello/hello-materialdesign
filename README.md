# Hello-MaterialDesign

Barebone react application for toying around with [Material Design](https://material.io/).

## Setup

* **Library:** [⚛️ ReactJS](https://reactjs.org/)
* **Transpiler:** [🐡 BabelJS](https://babeljs.io/)
* **Bundler:** [📦 ParcelJS](https://parceljs.org/)


## Run

```bash
npm install
npm start
```
